package Assignment3;

public class Faculty extends Employee {

	public Faculty(String name, String number, String employed, String salary, String weeks, String dept) {
		super(name, number, employed);
		
		
		double sa =Double.parseDouble(salary);
		setAnnualSalary(sa);
		
		
		int w=Integer.parseInt(weeks);
		setWeeksPerYears(w);
		
		
		setDepartment(dept);
		
		
		
	}

	private double annualSalary;
	private int weeksPerYears;
	private String department;
	
	
	
	public double getAnnualSalary() {
		return annualSalary;
	}



	public void setAnnualSalary(double annualSalary) {
		this.annualSalary = annualSalary;
	}



	public int getWeeksPerYears() {
		return weeksPerYears;
	}



	public void setWeeksPerYears(int weeksPerYears) {
		this.weeksPerYears = weeksPerYears;
	}



	public String getDepartment() {
		return department;
	}



	public void setDepartment(String department) {
		this.department = department;
	}



	@Override
	public double getPay() {
		// TODO Auto-generated method stub
		return (annualSalary/weeksPerYears)*2;
	}

}
