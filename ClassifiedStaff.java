package Assignment3;

public class ClassifiedStaff extends Employee {

	
	
	
	public ClassifiedStaff(String name, String number, String working, String salary, String div) {
		super(name, number, working);
		
		double salaryy=Double.parseDouble(salary);
		
		setWeeklySalary(salaryy);
		
		
		
		setDivision(div);
		// TODO Auto-generated constructor stub
	}



	private double weeklySalary;
	private String division;
	
	
	
	public double getWeeklySalary() {
		return weeklySalary;
	}



	public void setWeeklySalary(double weeklySalary) {
		this.weeklySalary = weeklySalary;
	}



	public String getDivision() {
		return division;
	}



	public void setDivision(String division) {
		this.division = division;
	}



	@Override
	public double getPay() {
		// TODO Auto-generated method stub
		return 2*weeklySalary;
	}

}
