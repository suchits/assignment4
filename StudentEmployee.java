package Assignment3;

public class StudentEmployee extends Employee {
	private int hoursWorked;
	private boolean isWorkStudy;
	private double payRate;

	public int getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(int hoursWorked) {
		this.hoursWorked = hoursWorked;
	}

	public boolean isWorkStudy() {
		return isWorkStudy;
	}

	public void setWorkStudy(boolean isWorkStudy) {
		this.isWorkStudy = isWorkStudy;
	}

	public double getPayRate() {
		return payRate;
	}

	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}

public StudentEmployee(String name, String number, String working, String hours, String workstudy, String rate){
//public Faculty(String name, String number, String employed, String salary, String weeks, String dept)
	super(name,number, working);
    
	
	int hour=Integer.parseInt(hours);
	setHoursWorked(hour);
	
	boolean workstud= Boolean.parseBoolean(workstudy);
	setWorkStudy(workstud);
	
	
	double rate_=Double.parseDouble(rate);
	setPayRate(rate_);
	
	

}

	@Override
	public double getPay() {
		// TODO Auto-generated method stub
		return hoursWorked*payRate;
	}

}
