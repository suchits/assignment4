package Assignment3;

public abstract class Employee {
	  public abstract double getPay();

private String employeeName;
private int employeeId;
private boolean isWorking;


public String getEmployeeName() {
	return employeeName;
}


public void setEmployeeName(String employeeName) {
	this.employeeName = employeeName;
}


public int getEmployeeId() {
	return employeeId;
}


public void setEmployeeId(int employeeId) {
	this.employeeId = employeeId;
}


public boolean getIsWorking() {
	return isWorking;
}


public void setIsWorking(boolean isWorking) {
	this.isWorking = isWorking;
}


public Employee(String name, String number, String employed) {

	setEmployeeName(name);
	
	
	int employeeId=Integer.parseInt(number);
	setEmployeeId( employeeId);
	
	
	boolean employ=Boolean.parseBoolean(employed);
	setIsWorking(employ);


	
}
}
